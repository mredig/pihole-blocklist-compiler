//
//  DownloadManager.swift
//  CompileBlocklists
//
//  Created by Michael Redig on 1/4/19.
//  Copyright © 2019 Michael Redig. All rights reserved.
//

import Foundation


struct DownloadManager {

	typealias DownloadProcessCompletionHandler = (_ fileSet: Set<String>, _ fromURL: URL) -> ()
	
	static func downloadLists(lists: Set<String>, completion: @escaping DownloadProcessCompletionHandler) {
		for list in lists {
			guard let url = URL(string: list) else {print("invalid URL: '\(list)'"); continue}
//			print("\(url) = \(list)")
			
			let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
				if error != nil {
					print("error retreiving from url: '\(url)', error: \(error!)")
				}
				let data = data ?? Data(capacity: 0)
				processListFile(fromURL: url, fileData: data, completion: completion)
			}
			task.resume()
			
		}
	}
	
	static func processListFile(fromURL: URL, fileData: Data, completion: @escaping DownloadProcessCompletionHandler) { //format from long string into array/set, remove comments, remove trailing spaces
		let hashName = fromURL.absoluteString.replacingOccurrences(of: "https://", with: "").replacingOccurrences(of: "http://", with: "").replacingOccurrences(of: "/", with: "-")
		let cacheCheck = ConfigLoader.checkCacheFile(named: hashName, withData: fileData)
		guard let fileString = String(data: cacheCheck, encoding: String.Encoding.ascii) else {fatalError("invalid data")}
		let blocks = fileString.splitOnNewlines()
		
		var tempSet = Set<String>()
		for block in blocks {
			guard let unCommented = block.strippedOfComments()?.lowercased() else {continue}
			let removeSpaces = unCommented.split(separator: " ").last
			let removeTabs = removeSpaces?.split(separator: "\t").last
			guard let cleanDomainSub = removeTabs else {continue}
			
			var cleanDomain = String(cleanDomainSub)
			cleanDomain = cleanDomain.strippedAfterNonValidDomain()
			guard isValidDomain(domain: cleanDomain) else {Logger.log("illegal domain: '\(cleanDomain)'"); continue}
			//check for regular ip address as well
			guard cleanDomain.isValidIPv4() == false else {Logger.log("is IP Address: '\(cleanDomain)'"); continue}
			tempSet.insert(cleanDomain)
		}
		completion(tempSet, fromURL)
	}
	
	static func isValidDomain(domain: String) -> Bool {
		
		//check that it has at least one .
		guard domain.rangeOfCharacter(from: CharacterSet(charactersIn: ".")) != nil else {return false}
		
		//check tld validity
		var tld = ""
		let revDom = domain.reversed()
		for char in revDom {
			if char == "." {
				break
			}
			tld = "\(char)\(tld)"
		}
		guard ConfigLoader.tlds.contains(tld) else {Logger.log("illegal tld - ", terminator: ""); return false}
		
		return true
	}
}
