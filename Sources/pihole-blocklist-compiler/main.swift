//
//  main.swift
//  CompileBlocklists
//
//  Created by Michael Redig on 1/3/19.
//  Copyright © 2019 Michael Redig. All rights reserved.
//

import Foundation



fileprivate var blocklistsFinished = 0
fileprivate var whitelistsFinished = 0
fileprivate let sortResults = true


func main() {
	Logger.verbosity = 1
	let (configDirectory, outputDirectory) = proceesArguments(args: CommandLine.arguments)

	_ = ConfigLoader(configDir: configDirectory) //need to run init to confirm conf directory and load lists
	DownloadManager.downloadLists(lists: ConfigLoader.blocklists) { (fileSet, fromURL) in //runs for EACH download list
		var lines = 0
		var dups = 0
		for cleanDomain in fileSet {
			if ConfigLoader.blacklist.contains(cleanDomain) {
				//track dupes
				dups += 1
			}
			ConfigLoader.blacklist.insert(cleanDomain)
			lines += 1
		}
		blocklistsFinished += 1
		let dupPrct = Float(dups) / Float(lines)
		Logger.log(verbosity: 1, "BL - [\(blocklistsFinished)/\(ConfigLoader.blocklists.count)] Dup%: \(dupPrct) lines: \(lines) compiled: \(ConfigLoader.blacklist.count) dups: \(dups) - \(fromURL)\n") //display stats
	}

	DownloadManager.downloadLists(lists: ConfigLoader.whitelists) { (whitelistFileSet, fromURL) in
		var lines = 0
		var dups = 0
		for cleanDomain in whitelistFileSet {
			if ConfigLoader.whitelist.contains(cleanDomain) {
				dups += 1
			}
			ConfigLoader.whitelist.insert(cleanDomain)
			lines += 1
		}
		whitelistsFinished += 1
		Logger.log(verbosity: 1, "WL - [\(whitelistsFinished)/\(ConfigLoader.whitelists.count)] lines: \(lines) compiled: \(ConfigLoader.whitelist.count) dups: \(dups) - \(fromURL)\n") //display stats

	}
	
	while blocklistsFinished != ConfigLoader.blocklists.count || whitelistsFinished != ConfigLoader.whitelists.count { //wait for downloads to finish
		let second = useconds_t(0.1 * 1000000);
		usleep(second)
	}
	
	Logger.log(verbosity: 1, "processing whitelists")
	ConfigLoader.blacklist = processWhitelist(with: ConfigLoader.whitelist, inSet: ConfigLoader.blacklist)

	Logger.log(verbosity: 1, "compiling final list")
	let finalList: [String]
	if sortResults == true {
		finalList = ConfigLoader.blacklist.sorted { (a, b) -> Bool in
			let aComp = a.split(separator: ".")
			let bComp = b.split(separator: ".")
	
			if aComp.last == bComp.last { // tld same
				guard let aCompany = aComp.dropLast().last, let bCompany = bComp.dropLast().last else {return false}
				if aCompany == bCompany { // domain name same
					guard let aSubDomain = aComp.dropLast().dropLast().last, let bSubDomain = bComp.dropLast().dropLast().last else {return false}
					return aSubDomain < bSubDomain //settle via subdomain, but dont try harder
				} else { // domain name differs
					return aCompany < bCompany
				}
			} else { //tlds differ
				guard let aLast = aComp.last, let bLast = bComp.last else { return false }
				return String(aLast) < String(bLast)
			}
		}
	} else {
		finalList = Array(ConfigLoader.blacklist)
	}

	Logger.log(verbosity: 1, "saving final list")
	
	let finalString = finalList.joined(separator: "\n")
	guard let finalData = finalString.data(using: .utf8) else {fatalError("failed conversion of final string to data")}
	let masterBlockURL = outputDirectory.appendingPathComponent("masterBlocklist").appendingPathExtension("txt")
	ConfigLoader.saveData(at: masterBlockURL, withData: finalData)

}


func proceesArguments(args: [String]) -> (configDir: URL, outputDirectory: URL) {
	if args.count <= 1 {
		dieUsage(with: nil)
	}
	let configPath = (args[1] as NSString).standardizingPath
	let configURL = URL(fileURLWithPath: configPath)
	if !FileManager.default.fileExists(atPath: configURL.path) {
		dieUsage(with: "invalid config path: '\(configURL.path)'")
		return (configURL, URL(fileURLWithPath: "")) //won't run, but required for swift happiness
	}
	let outputURL: URL
	if args.indices.contains(2) {
		let outputPath = (args[2] as NSString).standardizingPath
		outputURL = URL(fileURLWithPath: outputPath)
	} else {
		outputURL = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
	}
	return (configURL, outputURL)
}

func dieUsage(with error: String?) {
	if error != nil {
		print(error!)
	}
	print("Usage: pihole-blocklist-compiler /path/to/config/directory [optional/path/to/output/directory]")
	exit(1)
}

func processWhitelist(with whitelist: Set<String>, inSet compiledList: Set<String>) -> Set<String> { // just removes from blacklist
	var compiledList = compiledList
	for item in whitelist {
		if item.contains("*") {
			//has wildcard
			for domain in compiledList {
				if domain.matches(wildcardString: item) {
					compiledList.remove(domain)
					Logger.log(verbosity: 2, "whitelist: wildcard matched and removed \(domain)")
				}
			}
		} else {
			if compiledList.contains(item) {
				compiledList.remove(item)
				Logger.log(verbosity: 2, "whitelist: removed \(item)")
			}
		}
	}
	return compiledList
}




main()

