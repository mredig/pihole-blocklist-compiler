//
//  StringHelpers.swift
//  CompileBlocklists
//
//  Created by Michael Redig on 1/4/19.
//  Copyright © 2019 Michael Redig. All rights reserved.
//

import Foundation
import CryptoSwift



extension String {
	static let legalCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._"
	static let legalCharacterSet = CharacterSet(charactersIn: legalCharacters)
	static let illegalCharacterSet = legalCharacterSet.inverted

	
	func strippedOfComments() -> String? { //works on hashtag only
		let stripped = self.strippedOfCharacters(afterCharactersIn: CharacterSet(charactersIn: "#!"))
		if stripped.count > 0 {
			return stripped
		} else {
			return nil
		}
	}
	
	func strippedAfterNonValidDomain() -> String {
		return self.strippedOfCharacters(afterCharactersIn: String.illegalCharacterSet)
	}
	
	private func strippedOfCharacters(afterCharactersIn set: CharacterSet) -> String {
		
		var tStr = ""
		for char in self {
			if String(char).rangeOfCharacter(from: set) == nil {
				tStr.append(char)
			} else {
				break
			}
		}
		tStr.chomp()
		return tStr
	}
	
	mutating func chomp() { //remove trailing space (more than the real chomp)
		while (self.last == " " || self.last == "\t" || self.last == "\n" || self.last == "\r") {
			self.removeLast()
		}
	}
	
	func chomped() -> String {
		var newStr = self
		newStr.chomp()
		return newStr
	}
	
	func splitOnNewlines() -> [String] {
		let windowsIsStupid = self.ughWindowsNewlines()
//		let windowsIsStupid = self.replacingOccurrences(of: "\r", with: "") // fix windows line endings if they exist
		return windowsIsStupid.split(separator: "\n").compactMap{String($0)}
	}
	
	func ughWindowsNewlines() -> String { // replacingOccurrencesOf crashes on linux with \r for some reason, so doing a manual workaround
		guard let data = self.data(using: .utf8) else {fatalError("Couldn't convert string to data")}
		let byteArray = data.bytes
		var rStr = ""
		for character in byteArray {
			guard character != 13 else { continue } //utf8 \r is equal to 13
			rStr.append(Character(UnicodeScalar.init(character)))
		}
		return rStr
	}
	
	
	func isValidIPv4() -> Bool { //v4 only
		let digitSet = Set<Character>(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
		
		var digits = 0
		var dots = 0
		for char in self {
			if digitSet.contains(char) {
				digits += 1
			} else if char == "." {
				dots += 1
				digits = 0
			} else {
				return false //contained a non valid character
			}
			if digits > 3 || dots > 3 {
				return false //more than three digits in a row or too many decimals
			}
		}
		
		if dots < 3 {
			return false // didnt have enough separators
		}
		
		return true //everything passed
	}


	func matches(wildcardString: String) -> Bool {
		if !wildcardString.contains("*") && self.count != wildcardString.count {
			return false
		}
		let regex = wildcardString.replacingOccurrences(of: ".", with: "\\.").replacingOccurrences(of: "*", with: ".*")
		let strBool = self.replacingOccurrences(of: regex, with: "", options: .regularExpression, range: nil)

		return strBool == ""
	}
}

extension URL {
	enum Filestatus {
		case existsFile
		case existsDirectory
		case notExists
	}
	
	var filestatus: Filestatus {
		get {
			let filestatus: Filestatus
			var isDir: ObjCBool = false
			if FileManager.default.fileExists(atPath: self.path, isDirectory: &isDir) {
				if isDir.boolValue {
					// file exists and is a directory
					filestatus = .existsDirectory
				}
				else {
					// file exists and is not a directory
					filestatus = .existsFile
				}
			}
			else {
				// file does not exist
				filestatus = .notExists
			}
			return filestatus
		}
	}
}
