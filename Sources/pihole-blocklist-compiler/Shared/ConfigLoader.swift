//
//  ConfigLoader.swift
//  CompileBlocklists
//
//  Created by Michael Redig on 1/4/19.
//  Copyright © 2019 Michael Redig. All rights reserved.
//

import Foundation

fileprivate let downloadSizeThreshold = 0.8 // the new download must be at least 80% the file size of the cache in order to be considered updated

fileprivate let confFolderName = ".CompileBlocklists"
fileprivate var confFolderURL = URL(fileURLWithPath: ".")
fileprivate let cacheFolderName = "cache"
fileprivate var cacheFolderURL = confFolderURL.appendingPathComponent(cacheFolderName, isDirectory: true)

var getConfFolderURL: URL {
	return confFolderURL
}
var getCacheFolderURL: URL {
	return cacheFolderURL
}

fileprivate let permissions = [FileAttributeKey.posixPermissions: 0o750]



struct ConfigLoader {
	
	enum Lists: String { //plural is list of urls, singular is list of domains
		case blacklist
		case blocklists
		case whitelist
		case whitelists
		case tlds
	}
	

	let fm = FileManager.default

	static var blocklists = Set<String>()
	static var whitelist = Set<String>()
	static var whitelists = Set<String>()
	static var tlds = Set<String>()
	static var blacklist = Set<String>()
	
	init(configDir: URL) {
		confFolderURL = configDir
		cacheFolderURL = confFolderURL.appendingPathComponent(cacheFolderName, isDirectory: true)
		let confDirExists = confFolderURL.filestatus
		let cacheDirExists = cacheFolderURL.filestatus
		
		switch confDirExists {
		case .existsFile:
			fatalError("a file exists at \(confFolderURL.path) prevent the creation of the conf folder")
		case .notExists:
			do {
				try fm.createDirectory(at: cacheFolderURL, withIntermediateDirectories: true, attributes: permissions)
			} catch {
				fatalError("could not generate conf/cache folder \(error)")
			}
			Logger.log(verbosity: 1, "created conf dir")
			ConfigLoader.createDefaultBlocklist()
			ConfigLoader.createDefaultWhitelist()
		default:
			break
		}
		
		switch cacheDirExists {
		case .notExists:
			do {
				try FileManager.default.createDirectory(at: cacheFolderURL, withIntermediateDirectories: true, attributes: nil)
			} catch {
				fatalError("couldn't create directory '\(cacheFolderURL.path)': \(error)")
			}
		default:
			break
		}

		//load lists here
		
		ConfigLoader.blocklists = ConfigLoader.loadList(list: .blocklists)
		ConfigLoader.whitelist = ConfigLoader.loadList(list: .whitelist)
		ConfigLoader.whitelists = ConfigLoader.loadList(list: .whitelists)
		ConfigLoader.blacklist = ConfigLoader.loadList(list: .blacklist)
		ConfigLoader.tlds = ConfigLoader.latestTLDs()
	}
	

	static func createDefaultBlacklist() {
		guard let data = PresetStrings.presetBlacklist.data(using: .utf8) else {fatalError("failed converting string to data")}
		ConfigLoader.saveData(at: confFolderURL.appendingPathComponent(ConfigLoader.Lists.blacklist.rawValue), withData: data)
	}
	
	static func createDefaultBlocklist() {
		ConfigLoader.saveList(as: confFolderURL.appendingPathComponent(ConfigLoader.Lists.blocklists.rawValue), PresetStrings.presetBlocklists)
	}
	
	static func createDefaultWhitelist() {
		ConfigLoader.saveList(as: confFolderURL.appendingPathComponent(ConfigLoader.Lists.whitelist.rawValue), PresetStrings.presetWhitelist)
	}
	
	static func createDefaultWhitelists() {
		ConfigLoader.saveList(as: confFolderURL.appendingPathComponent(ConfigLoader.Lists.whitelists.rawValue), [String]())
	}
	
	static func createDefaultTLDList() {
		guard let data = PresetStrings.presetTLDs.data(using: .utf8) else {fatalError("failed converting string to data")}
		ConfigLoader.saveData(at: confFolderURL.appendingPathComponent(ConfigLoader.Lists.tlds.rawValue), withData: data)
	}
	
	static func saveList(as saveURL: URL, _ list: [String]) {
		let fm = FileManager.default

		let allLines = list.joined(separator: "\n") as NSString
		do {
			try allLines.write(to: saveURL, atomically: true, encoding: String.Encoding.utf8.rawValue)
			try fm.setAttributes(permissions, ofItemAtPath: saveURL.path)
		} catch {
			fatalError("Couldn't save default \(saveURL.lastPathComponent): \(error)")
		}
	}
	
	static func loadList(list: Lists, cleanList: Bool = true) -> Set<String> {
		let loadURL = confFolderURL.appendingPathComponent(list.rawValue)
		
		let fileExists = loadURL.filestatus
		if fileExists == .notExists {
			switch list {
			case .blacklist:
				createDefaultBlacklist()
			case .blocklists:
				createDefaultBlocklist()
			case .whitelist:
				createDefaultWhitelist()
			case .whitelists:
				createDefaultWhitelists()
			case .tlds:
				createDefaultTLDList()
			}
			
		}
		
		let data: Data
		do {
			data = try Data(contentsOf: loadURL)
		} catch {
			fatalError("could not load \(list.rawValue): \(error)")
		}
		
		guard let longString = String(data: data, encoding: .utf8) else { fatalError("could not convert data of \(list.rawValue) to string")}
		let subStrs = longString.split(separator: "\n").compactMap{String($0).ughWindowsNewlines()}
//		print(subStrs)
		
		if cleanList {
			let noComments = subStrs.compactMap{$0.strippedOfComments()?.chomped()}
			return Set(noComments)
		} else {
			return Set(subStrs.compactMap{String($0)})
		}
	}
	
	static func checkCacheFile(named name: String, withData data: Data) -> Data {
		
		let loadURL = cacheFolderURL.appendingPathComponent(name).appendingPathExtension("txt")
		
		let fileExists = loadURL.filestatus
		switch fileExists {
		case .existsFile:
			// compare files
			let dataSize = data.count
			let cacheData: Data
			do {
				cacheData = try Data(contentsOf: loadURL)
			} catch {
				fatalError("couldn't load cache for \(name): \(error)")
			}
			let cacheSize = cacheData.count
			let downloadRelativeSize = Double(dataSize) / Double(cacheSize)
			Logger.log(verbosity: 2, "this download is \(downloadRelativeSize)x the size of the cache")
			if downloadRelativeSize >= downloadSizeThreshold {
				//save download in cache
				Logger.log(verbosity: 3, "\(name) within threshold - saving")
				saveData(at: loadURL, withData: data)
				return data
			} else {
				Logger.log(verbosity: 3, "\(name) had too big of a change - not saving")
				return cacheData
			}
			
		case .notExists:
			//just save what there is
			saveData(at: loadURL, withData: data)
			return data
		case .existsDirectory:
			fatalError("aimed for cache file '\(name)', but got a directory instead")
		}
	}
	
	static func saveData(at saveURL: URL, withData data: Data) {
		do {
			try data.write(to: saveURL)
		} catch {
			fatalError("failed writing \(saveURL): \(error)")
		}
	}

	static func latestTLDs() -> Set<String> {
		var tldDownloaded = false
		
		let tldURLString = "http://data.iana.org/TLD/tlds-alpha-by-domain.txt"
		guard let url = URL(string: tldURLString) else {fatalError("invalid URL: '\(tldURLString)'")}
		var theTLDs = Set<String>()
		let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
			guard error == nil else {fatalError("\(error!)")}
			guard var data = data else {Logger.log(verbosity: 1, "download data failed");return}
			data = checkCacheFile(named: url.path.sha1(), withData: data)
			guard let tldFileString = String(data: data, encoding: .utf8) else {tldDownloaded = true; return}
			let tldArray = tldFileString.splitOnNewlines().compactMap{$0.strippedOfComments()?.lowercased()}
			theTLDs = Set(tldArray)
			tldDownloaded = true
		}
		task.resume()
		
		while tldDownloaded != true { //wait for tlds download to finish
			let second = useconds_t(0.1 * 1000000);
			usleep(second)
		}

		return theTLDs
	}
}
