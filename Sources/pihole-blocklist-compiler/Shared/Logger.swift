//
//  Logger.swift
//  CompileBlocklists
//
//  Created by Michael Redig on 1/7/19.
//  Copyright © 2019 Michael Redig. All rights reserved.
//

import Foundation

struct Logger {
	static var verbosity = 0
	static func log(verbosity threshold: Int = 3, _ args: Any..., terminator: String = "\n") {
		if verbosity >= threshold {
			print(args, terminator: terminator)
		}
	}
}
