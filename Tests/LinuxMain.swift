import XCTest

import pihole_blocklist_compilerTests

var tests = [XCTestCaseEntry]()
tests += pihole_blocklist_compilerTests.allTests()
XCTMain(tests)