import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(pihole_blocklist_compilerTests.allTests),
    ]
}
#endif