import XCTest
import class Foundation.Bundle

final class pihole_blocklist_compilerTests: XCTestCase {
	
	func testStringReplace() {
		
		let test3 = "windows\r\nis\r\nstupid"
		let new3 = test3.ughWindowsNewlines()
		XCTAssertTrue(new3 == "windows\nis\nstupid", "actually made: \(new3)")
	}
	
	func testMatchWildcard() {
		let baseStr = "abcdefgefg"
		let match1 = "*efg"
		let match2 = "abc*"
		let match3 = "abc*efg"
		let match4 = "a*bcdefgefg"
		let match5 = "a*g"
		let match6 = "**g"
		let match7 = "a**"
		let match8 = "abc*ge*"
		
		let noMatch1 = "xyz"
		let noMatch2 = "abc*efgefgefg"
		let noMatch3 = "abc*efG"
		
		let baseStr2 = "youtube-stuff-r----------3-4-32-x-dsa-f.googlevideo.com"
		let baseStr3 = "compromised-site-googlevideo.com"
		
		let match2not3 = "*.googlevideo.com"

		XCTAssertTrue(baseStr.matches(wildcardString: match1))
		XCTAssertTrue(baseStr.matches(wildcardString: match2))
		XCTAssertTrue(baseStr.matches(wildcardString: match3))
		XCTAssertTrue(baseStr.matches(wildcardString: match4))
		XCTAssertTrue(baseStr.matches(wildcardString: match5))
		XCTAssertTrue(baseStr.matches(wildcardString: match6))
		XCTAssertTrue(baseStr.matches(wildcardString: match7))
		XCTAssertTrue(baseStr.matches(wildcardString: match8))
		
		XCTAssertFalse(baseStr.matches(wildcardString: noMatch1))
		XCTAssertFalse(baseStr.matches(wildcardString: noMatch2))
		XCTAssertFalse(baseStr.matches(wildcardString: noMatch3))

		XCTAssertTrue(baseStr2.matches(wildcardString: match2not3))
		XCTAssertFalse(baseStr3.matches(wildcardString: match2not3))
	}

    /// Returns path to the built products directory.
    var productsDirectory: URL {
      #if os(macOS)
        for bundle in Bundle.allBundles where bundle.bundlePath.hasSuffix(".xctest") {
            return bundle.bundleURL.deletingLastPathComponent()
        }
        fatalError("couldn't find the products directory")
      #else
        return Bundle.main.bundleURL
      #endif
    }

    static var allTests = [
        ("testStringReplace", testStringReplace),
    ]
}
